import './App.scss';
import { Navigate, Routes, Route } from "react-router-dom";
import { useSelector } from "react-redux";

import { properties } from "./properties";
import Footer from "./components/Footer";
import MobileMenu from "./components/MobileMenu";
import PropertiesList from "./components/PropertiesList";

const App = () => {
  const open = useSelector(state => state.mobileMenuOpen);

  return (
    <>
      <MobileMenu open={open} />

      <div className="content">
        <Routes>
          <Route path="*" element={<PropertiesList properties={properties} />} />
        </Routes>
      </div>

      <Footer />
    </>
  );
}

export default App;
