const initialState = {
  mobileMenuOpen: false,
}

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case "toggle":
      return {
        ...state,
        mobileMenuOpen: !state.mobileMenuOpen,
      };

    default:
      return state;
  }
}

export default appReducer;
