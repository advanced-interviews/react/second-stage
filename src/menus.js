export const menus = [
  {
    text: "Properties",
    url: "/properties",
  },
  {
    text: "About Us",
    url: "/about",
  },
  {
    text: "Contact Us",
    url: "/contact",
  },
];
