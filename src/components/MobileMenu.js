import React from "react";

const MobileMenu = (props) => {
  if (!props.open) {
    return null;
  }

  return (
    <menu>
      <li>
        <a href="/properties">
          Properties
        </a>
      </li>
      <li>
        <a href="/about">
          About Us
        </a>
      </li>
      <li>
        <a href="/contact">
          Contact Us
        </a>
      </li>
    </menu>
  )
}

export default MobileMenu;
