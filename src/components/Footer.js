import React from "react";

import { ReactComponent as LogoIcon } from "../images/logo.svg";

const Footer = (props) => {
  return (
    <footer>
      <div>
        Email: info@advanced.co.uk
      </div>

      <div>
        <LogoIcon className="logo" />
      </div>

      <div>
        Phone: 0345 512 0208
      </div>
    </footer>
  )
}

export default Footer;
