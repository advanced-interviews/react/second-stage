import React from "react";

import { ReactComponent as MenuIcon } from "../images/menu.svg";

const MenuSwitcher = () => {

  const handleOnClick = () => {

  }

  return (
    <>
      <MenuIcon
        className="menu-switcher"
        width={20}
        height={20}
        onClick={handleOnClick}
      />
    </>
  )
}

export default MenuSwitcher;
