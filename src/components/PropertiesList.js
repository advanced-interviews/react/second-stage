import React from "react";

const PropertiesList = (props) => {
  return (
    <div className="properties-list">
      { props.properties.map(property => (
        <div className="property-box">
          <div className="property-image">
            <img src={property.image} />
          </div>
          <div className="property-details">
            <h3 className="property-address">{ property.address }</h3>

            <div>
              <strong className="property-price">Price: £{ property.price.toLocaleString() }</strong>

              <p className="property-description">{ property.description }</p>

              <div className="property-actions">
                <button className="secondary">
                  Call Us
                </button>
                <button className="primary">
                  View Property
                </button>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}

export default PropertiesList;
