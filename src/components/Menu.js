import React from "react";

import { menus } from "../menus";

const Menu = (props) => {
  return (
    <menu>
      { menus.map(menu => (
        <li>
          <a href={menu.url}>
            { menu.text }
          </a>
        </li>
      ))}
    </menu>
  );
}

export default Menu;
