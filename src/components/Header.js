import React from "react";

import Menu from "./Menu";
import MenuSwitcher from "./MenuSwitcher";

import { ReactComponent as LogoIcon } from "../images/logo.svg";

const Header = (props) => {
  return (
    <header>
      <div>
        <LogoIcon className="logo" />
      </div>

      <MenuSwitcher />

      <Menu />
    </header>
  )
}

export default Header;
