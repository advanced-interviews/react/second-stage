export const properties = [
  {
    image: "/images/property_1.jpg",
    address: "123 Fake Street, Faketown, Fakeville",
    price: 250000,
    description: "Lorem ipsum dolor sit amet. Aut incidunt omnis sed autem illo ut iure explicabo qui enim mollitia. Sed consequatur aliquid ea porro repudiandae hic velit deleniti? Et delectus molestiae est excepturi veritatis aut reprehenderit rerum. Qui ipsa nisi aut porro quisquam ad animi dolores in cupiditate magnam sit possimus sequi et tenetur quasi est rerum aliquid. Quo odio cumque aut esse aspernatur qui dolore quod et consequatur autem non laborum omnis At sint dolor? Et dolorum temporibus id illo tempore in perferendis optio sed eligendi dicta est sapiente doloribus et enim neque cum sint voluptatibus. Qui nostrum natus est voluptas reprehenderit in voluptas quod vel fugit laudantium. Aut expedita vero non adipisci tenetur sit nisi quis vel internos totam sit magnam accusamus et ducimus voluptatem.",
  },
  {
    image: "/images/property_2.jpg",
    address: "123 Some Street, Sometown, Someville",
    price: 150000,
    description: "Lorem ipsum dolor sit amet. Aut incidunt omnis sed autem illo ut iure explicabo qui enim mollitia. Sed consequatur aliquid ea porro repudiandae hic velit deleniti? Et delectus molestiae est excepturi veritatis aut reprehenderit rerum. Qui ipsa nisi aut porro quisquam ad animi dolores in cupiditate magnam sit possimus sequi et tenetur quasi est rerum aliquid. Quo odio cumque aut esse aspernatur qui dolore quod et consequatur autem non laborum omnis At sint dolor? Et dolorum temporibus id illo tempore in perferendis optio sed eligendi dicta est sapiente doloribus et enim neque cum sint voluptatibus. Qui nostrum natus est voluptas reprehenderit in voluptas quod vel fugit laudantium. Aut expedita vero non adipisci tenetur sit nisi quis vel internos totam sit magnam accusamus et ducimus voluptatem.",
  },
];
