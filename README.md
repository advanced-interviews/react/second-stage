# Advanced.co.uk ReactJS Technical Interview

### Tasks

1. Install all required dependencies.
2. Run the application on port 3000.
3. Load the Header component in the App component.
4. Style the links in the header navigation to be; bold, color: #0a1044 and font size 18px.
5. Make the email address and phone number in the footer clickable links which will either make a phone call or open a new email.
6. Add hover effects to the 'Call Us' and 'View Property' buttons located under the property description. The hover affect should reduce the size to 90% of it's original size.
7. Create a new blank 'Contact Us' page, the URL for this page is '/contact'.
8. Add a simple form to the 'Contact Us' page, consisting of the following fields:
  - Name
  - Email
  - Message
  - Submit button
9. When the submit button is clicked and all fields are completed, show a confirmation message.
10. Make the 'Contact Us' form responsive, it must work on desktop (1400px wide), tablet devices (1180px x 820px, iPad Air) or mobile devices (390px x 844px, iPhone 12 Pro).
11. Complete the 'handleOnClick' function in the MenuSwitcher component (src/components/MenuSwitcher.js) to update the 'mobileMenuOpen' property in the Redux state.

Bonus Point: Can you explain why Redux requires the 'default' case in the 'src/store/reducer.js' file?
